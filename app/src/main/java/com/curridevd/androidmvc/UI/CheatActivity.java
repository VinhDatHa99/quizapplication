package com.curridevd.androidmvc.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.curridevd.androidmvc.R;

public class CheatActivity extends AppCompatActivity {

	private  static final String EXTRA_ANSWER_IS_TRUE =
			"com.curridevd.androidmvc.MainActivity.answer_is_true";
	private static  final String EXTRA_ANSWER_SHOWN =
			"com.curridevd.androidmvc.show_answer";
	private boolean answerIsTrue;
	private TextView txtAnswer;
	private Button btnShowAnswer;
	public static Intent newIntent(Context packageContext, boolean answerIsTrue)
	{
		Intent intent = new Intent(packageContext,CheatActivity.class);
		intent.putExtra(EXTRA_ANSWER_IS_TRUE,answerIsTrue);
		return intent;
	}

	public static boolean wasAnswerShown(Intent result)
	{
		return result.getBooleanExtra(EXTRA_ANSWER_SHOWN,false);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cheat);
		answerIsTrue =getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE,false);

		txtAnswer = findViewById(R.id.textviewAnswer);
		btnShowAnswer = findViewById(R.id.showAnswerButton);
		btnShowAnswer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (answerIsTrue) {
					txtAnswer.setText(R.string.true_button);
				} else {
					txtAnswer.setText(R.string.false_button);
				}
				setAnswerShowResult(true);
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				{
				int cx = btnShowAnswer.getWidth() / 2;
				int cy = btnShowAnswer.getHeight() / 2;
				float radius = btnShowAnswer.getWidth();
				Animator anim = ViewAnimationUtils.createCircularReveal(btnShowAnswer, cx, cy, radius, 0);

				anim.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						super.onAnimationEnd(animation);
						txtAnswer.setVisibility(View.VISIBLE);
					}
				});
				anim.start();
				}else {
					txtAnswer.setVisibility(View.VISIBLE);
					btnShowAnswer.setVisibility(View.VISIBLE); }
			}

		});
	}

	private void setAnswerShowResult(boolean isAnswerShown)
	{
		Intent data =new Intent();
		data.putExtra(EXTRA_ANSWER_SHOWN,isAnswerShown);
		setResult(RESULT_OK,data);
	}
}