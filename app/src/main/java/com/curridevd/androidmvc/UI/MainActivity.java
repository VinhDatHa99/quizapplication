package com.curridevd.androidmvc.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.curridevd.androidmvc.R;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = "MainActivity";
	private static final String KEY_INDEX = "index";
	private static final int REQUEST_CODE_CHEAT = 0;
	private TextView txtScore;
	private Button btnTrue;
	private Button btnFalse;
	private Button btnNext, btnPrevious;
	private TextView txtQuestion;
	private Button btnCheat;
	private int currentIndex = 0;
	private boolean isCheater;
	private Question[] QuestionBank = new Question[]
		{
			new Question(R.string.question_VietNam,true),
			new Question(R.string.question_africa,true),
			new Question(R.string.question_asia,false),
			new Question(R.string.question_country,true),
			new Question(R.string.question_ocean,false),
			new Question(R.string.question_tech,false)
		};
	//Add previous feature
	private void updateQuestion()
	{
		int question = QuestionBank[currentIndex].getTextResId();
		txtQuestion.setText(question);
	}
	/*
	Accept boolean variable that identifies whether the user pressed TRUE or FALSE
	Then, it will check the user's answer with the answer's in the current Question object then make a toast display
	the message
	 */
	private void checkAnswer(boolean userPressTrue)
	{
		boolean answerIsTrue = QuestionBank[currentIndex].isAnswerTrue();
		int messageRedID = 0;
		if (isCheater)
		{
			messageRedID = R.string.judgment_toast;
		}
		else {
			if (userPressTrue == answerIsTrue) {
				messageRedID = R.string.correct_toast;
			} else messageRedID = R.string.incorrect_toast;
		}
		Toast.makeText(this,messageRedID,Toast.LENGTH_SHORT).show();
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG,"onCreate(Bundle) called");

		setContentView(R.layout.activity_main);
		Mapping();
		//Set display question
//		int question = QuestionBank[currentIndex].getTextResId();
//		txtQuestion.setText(question);

		btnTrue.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view)
			{
				checkAnswer(true);
			}
		}
		);
		btnFalse.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				checkAnswer(false);
			}
		}
		);
		//Encapsulating with updateQuestion() so we will delete codes on line 36,37,68,69
		btnNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				currentIndex = (currentIndex + 1) % QuestionBank.length;
				isCheater	 = false;
			updateQuestion();
			}
		});
		btnPrevious.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if (currentIndex == 0)
				{
					currentIndex = (QuestionBank.length -1) % QuestionBank.length;
				}else
				{
					currentIndex = (currentIndex - 1) % QuestionBank.length;
				}
				updateQuestion();
			}
		});
		if (savedInstanceState != null)
		{
			currentIndex = savedInstanceState.getInt(KEY_INDEX,0);
		}

		btnCheat.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//Intent startCheatActivity = new Intent(MainActivity.this, CheatActivity.class);
				boolean answerIsTrue = QuestionBank[currentIndex].isAnswerTrue();
				Intent startCheatActivity = CheatActivity.newIntent(MainActivity.this,answerIsTrue);
				//startActivity(startCheatActivity);
				//get result back from CheatActivity
				startActivityForResult(startCheatActivity,REQUEST_CODE_CHEAT);
			}
		});
		updateQuestion();
	}
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
		if(resultCode != Activity.RESULT_OK)
		{
			return ;
		}
		if (requestCode == REQUEST_CODE_CHEAT)
		{
			if (data == null)
			{
				return;
			}
			isCheater = CheatActivity.wasAnswerShown(data);
		}
	}


	@Override
	public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		Log.i(TAG,"onSaveInstanceState");
		savedInstanceState.putInt(KEY_INDEX,currentIndex);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Log.d(TAG,"onStart() called");
	}
	@Override
	public void onPause()
	{
		super.onPause();
		Log.d(TAG,"onPause() called");
	}
	@Override
	public void onResume()
	{
		super.onResume();
		Log.d(TAG,"onResume() called");
	}
	@Override
	public void onStop()
	{
		super.onStop();
		Log.d(TAG,"onStop() called");
	}
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.d(TAG,"onDestroy() called");
	}

	public void Mapping ()
	{
		btnCheat	= findViewById(R.id.buttonCheat);
		btnTrue 	= findViewById(R.id.trueButton);
		btnFalse 	= findViewById(R.id.falseButton);
		txtQuestion = findViewById(R.id.textviewQuestion);
		btnNext		= findViewById(R.id.buttonNext);
		btnPrevious	= findViewById(R.id.buttonPrevious);
	}
}