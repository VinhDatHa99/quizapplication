package com.curridevd.androidmvc.UI;

public class Question {
	private int TextResId;
	private boolean AnswerTrue;

	public Question(int TextResId, boolean AnswerTrue) {
		this.TextResId = TextResId;
		this.AnswerTrue = AnswerTrue;
	}

	public int getTextResId() {
		return TextResId;
	}

	public void setTextResId(int TextResId) {
		this.TextResId = TextResId;
	}

	public boolean isAnswerTrue() {
		return AnswerTrue;
	}

	public void setAnswerTrue(boolean answerTrue) {
		AnswerTrue = answerTrue;
	}
}
